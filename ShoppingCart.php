<?php

namespace Apple\Checkout;

class ShoppingCart {
  protected $lineItems = [];

  public function add($lineItem)
  {
    $this->lineItems[] = $lineItem;
  }

  public function getTotal()
  {
    $total = 0;

    foreach($this->lineItems as $lineItem) {
      $total += $lineItem->getTotal();
    }

    return $total;
  }
}
